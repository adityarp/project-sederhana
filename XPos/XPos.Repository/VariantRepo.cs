﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.DataModel;

namespace XPos.Repository
{
    public class VariantRepo
    {
        //List
        public static List<Variant> All()
        {
            List<Variant> result = new List<Variant>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Include("Category")
                    .ToList();
            }
            return result;
        }
        //List by Ctegory
        public static List<Variant> ByCategory(long catId)
        {
            //catId => Category Id
            List<Variant> result = new List<Variant>();
            using (var db=new XPosContext())
            {
                result = db.Variants
                    //.Include("Category")
                    .Where(o => o.CategoryId == (catId != 0 ? catId : o.CategoryId))
                    .ToList();
            }
            return result;
        } 
        //Create
        public static ResponseResult Update(Variant entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    if (entity.Id == 0)
                    {
                        // Create
                        entity.CreateBy = "Aditya";
                        entity.CreateDate = DateTime.Now;
                        db.Variants.Add(entity);
                        db.SaveChanges();

                        result.Message = "Created";
                        result.Entity = entity;

                    }
                    else
                    {
                        //Edit
                        Variant variant = db.Variants
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                        if (variant == null)
                        {
                            result.Success = false;
                        }
                        else
                        {
                            variant.CategoryId = entity.CategoryId;

                            variant.Initial = entity.Initial;
                            variant.Name = entity.Name;
                            variant.Active = entity.Active;
                            variant.ModifBy = "Aditya";
                            variant.ModifDate = DateTime.Now;
                            db.SaveChanges();

                            result.Message = "Updated";
                            result.Entity = entity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }
        public static Variant ById(long id)
        {
            Variant result = new Variant();
            try
            {
                using (var db = new XPosContext())
                {
                    result = db.Variants
                        .Where(o => o.Id == id)
                        .FirstOrDefault();
                    //if (result == null)
                    //{
                    //    result = new Category();
                    //}
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new Variant() : result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {

                using (var db = new XPosContext())
                {
                    Variant variant = db.Variants
                        .Where(o => o.Id == id)
                        .FirstOrDefault();

                    Variant oldvariant = variant;
                    result.Entity = oldvariant;

                    if (variant != null)
                    {
                        db.Variants.Remove(variant);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
