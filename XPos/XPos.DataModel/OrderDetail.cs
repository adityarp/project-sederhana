namespace XPos.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderDetail")]
    public partial class OrderDetail
    {
        public long Id { get; set; }

        public long HeaderId { get; set; }

        public long ProductId { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public bool Active { get; set; }

        [Required]
        [StringLength(50)]
        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        [StringLength(50)]
        public string ModifBy { get; set; }

        public DateTime? ModifDate { get; set; }

        public virtual OrderHeader OrderHeader { get; set; }

        public virtual Product Product { get; set; }
    }
}
