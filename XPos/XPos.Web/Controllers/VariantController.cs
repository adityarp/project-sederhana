﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.DataModel;
using XPos.Repository;

namespace XPos.Web.Controllers
{
    public class VariantController : Controller
    {
        // GET: Variant
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            List<Variant> list = VariantRepo.All();
            return PartialView("_List", list);
        }
        //Get
        public ActionResult Create()
        {
            //Parent List ==> Category
            ViewBag.ParentList = new SelectList(CategoryRepo.All(), "Id", "Name");
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(Variant model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }
        //Get
        public ActionResult Edit(long id)
        {
            //Cara 1
            //Category category = CategoryRepo.ById(id);
            //return PartialView("_Edit", category);

            //Cara 2
            return PartialView("_Edit", VariantRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(Variant model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", VariantRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(Variant model)
        {
            ResponseResult result = VariantRepo.Delete(model.Id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);

        }
        public ActionResult ByCategory(long id = 0)
        {
            //id => Category Id
            List<Variant> list = VariantRepo.ByCategory(id);
            return PartialView("_ByCategory", list);
        }
    }
}